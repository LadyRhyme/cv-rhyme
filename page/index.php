<!DOCTYPE html>
<html lang="en">

<head>
    <title>CV</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="../stylesheet/style.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>

<body>

    <!-- NAV BAR DEBUT -->
    
    <?php
    include ('nav.php')
      ?>

        <!-- NAV BAR END -->
        <!-- INTRO DEBUT-->
        <main class="content">
            <div class="content_inner" style="text-align: center;">
                <h1>Andréa SKEGRO</h1>
                <br>
                <div class="social">
                    <a href="https://twitter.com/ladyrhyme_/"><i class="fa fa-twitter"></i></a>
                    <a href="https://twitter.com/ladyrhyme_/"></a><i class="fa fa-linkedin"></i></a>
                    <a href="https://gitlab.com/LadyRhyme"></a><i class="fa fa-gitlab"></i></a>
                    <a href="https://twitter.com/ladyrhyme_/"></a><i class="fa fa-github"></i></a>
                </div>
                <br>
        <!-- INTRO END -->
        <!-- CAROUSELL DEBUT-->

        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="../media/deadcells.jpg" class="d-block w-100" alt="">
                <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </div>
              </div>
              <div class="carousel-item">
                <img src="../media/foret.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                  <h5>Second slide label</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
              </div>
              <div class="carousel-item">
                <img src="../media/montagne.png" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                  <h5>Third slide label</h5>
                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        <!-- CAROUSELL FIN-->
              <br><br>

              <img src="../media/profile.jpg" alt="avatar" class="rounded-circle" style="text-align: center;">
              <br><hr>
                <br>
                <p style="text-align: center;">Jeune femme de 23 ans, passionnée de photographie, graphisme, illustration, culture asiatique
traditionnelles, de modélisation 3D, création de Site Web et de programmation informatique.

                <p style="text-align: center;"><b>Parles couramment</b> : Anglais, Serbo-Croate</p>
                <p style="text-align: center;"><b>Logiciels</b> : Illustrator, Photoshop, In Design, Autodesk, Cinema 4D, Fusion360, Microsoft Office</p>
                <br><i class="fa fa-html5 fa-4x"></i><i class="fa fa-css3 fa-4x"></i><i class="fa fa-code fa-4x"></i>
            </div>
        </main>

    </div>

<?php include 'Footer.php' ?>

</body>
</html>