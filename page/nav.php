<div class="page">
        <span class="menu_toggle">
                <i class="menu_open fa fa-bars fa-lg"></i>
                <i class="menu_close fa fa-times fa-lg"></i>
            </span>

        <ul class="menu_items">
            <li><a href="../page/index.php"><i class="fa fa-home fa-2x">
                    </i>
                    Index</a>
            </li>

            <li><a href="../page/about.php"><i class="fa fa-briefcase fa-2x">
                      </i>
                       Projet</a>
            </li>

            <li><a href="../page/cv.php"><i class="fa fa-star fa-2x">
                      </i>
                        Parcours</a>
            </li>

            <li><a href="../page/contact.php"><i class="fa fa-phone fa-2x" style="padding-left: 130px;">
                      </i>
                        Contact</a>
            </li>

        </ul>

        <script>
        var $page = $('.page');
        $('.menu_toggle').on('click', function() {
            $page.toggleClass('real');
        });

        $('.content').on('click', function() {
            $page.removeClass('real');
        });
    </script>