<!doctype html>
<html lang="en">
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <title>Formation et Expérience</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="../stylesheet/style.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Optional JavaScript // MAP -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        </head>

<?php include ('nav.php')
?>
<body>
<main class="content">
            <div class="content_inner" style="text-align: center;">

            <h1><a class="text-secondary">Contactez-moi</a></h1>
            <p>L'essayez c'est l'adopter ! Le meilleure moyen de me contacter c'est par les différents moyens ci-dessous :</p>
            <p style="text-align: center";><i class="fa fa-envelope fa-1x"></i> ladyrhyme.art<b style="color: #F47272";>@</b>gmail.com</p>
            <p style="text-align: center";><i class="fa fa-phone fa-1x"></i> 07.83.30.78.68</p>

            <form>
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInput">Name</label>
      <input type="text" class="form-control mb-2" id="inlineFormInput" placeholder="Nom/Prénom">
    </div>
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInputGroup">Username</label>
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">@</div>
        </div>
        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Mail">
      </div>
    </div>
    <div class="col-auto">
      <button type="submit" class="btn btn-secondary mb-2">Soumettre</button>
    </div>
  </div>
  <div class="mb-3">
    <label for="validationTextarea"></label>
    <textarea class="form-control" placeholder="Merci de vouloir entrer un message." required></textarea>
    <div class="invalid-feedback">
    </div>
    </div>
  </div>
  </form>

</div>
</div>
</body>
</html>