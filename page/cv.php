<!doctype html>
<html lang="en">
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <title>Formation et Expérience</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="../stylesheet/style.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        </head>
  
<body>

<?php
    include ('nav.php')
      ?>   
    <div class="content">
        <div class="content_inner" style="text-align: center;">
            <img src="../media/awardicon.png" alt="formation" style="width: 75px;">
            <h1><a class="text-secondary">Formation</a></h1>
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg">
                        <i class="fa fa-star fa-2x"></i><h2>1er CSMI</h2>
                        <p>- ENAAI | Technolac</p>
                        <p style="font-size: 12px;">2015/2016</p>
                        <p style="font-size: 12px;">Art traditionnel et digital, sculpture, perspective, graphisme, utilisation de logiciel de traitement, d’image et création</p>
                    </div>
                    <div class="col-lg">
                        <i class="fa fa-star fa-2x"></i><h2>Bac Pro Gestion-Administrative</h2>
                        <p>- Lycée Sainte-Geneviève | Chambéry</p>
                        <p style="font-size: 12px;">2012/2015</p>
                        <p style="font-size: 12px;">Gestion des comptes entreprises, fiche de paies, appels, téléphoniques, création, et gestion de projet, courrier</p>
                    </div>
                    </div>
                </div>

                <img src="../media/walleticon.png" alt="experiences" style="width: 75px;">
                <h1><a class="text-secondary">Expériences Professionelles</a></h1>
                <div class="container">
                    <div class="row text-center">
                        <div class="col-lg">
                            <i class="fa fa-star fa-2x"></i><h2>Appuie E-Administrative</h2>
                            <p>Mission Intérim 3 mois</p>
                            <p>- ENEDIS | Pôle Chambéry</p>
                            <p style="font-size: 12px;">2019</p>
                            <p style="font-size: 12px;">Attachements De Factures, Commandes Travaux, Relation Avec Les Clients Et Prestataires
                                Par Téléphone Et Mail, Complétude De Dossier, Relances Prestataires Et Clients, Suivi
                                Dossier Client, Traites Des Mise En Services Compteur</p>
                        </div>
                        <div class="col-lg">
                            <i class="fa fa-star fa-2x"></i><h2>Animatrice Web et Multimedia</h2>
                            <p>Service civique 3 mois</p>
                            <p>- Mission Locale Jeunes | Chambéry</p>
                            <p style="font-size: 12px;">2019/2018</p>
                            <p style="font-size: 12px;">Animation sur les plateformes numérique et web, création et modification de CV, animation de collectifs, flux, création graphique, promotions réseaux sociaux</p>
                        </div>
                        <div class="col-lg">
                            <i class="fa fa-star fa-2x"></i><h2>Assistante Commerciale et Marketing</h2>
                            <p>Mission Intérim 1 mois</p>
                            <p style="font-size: 12px;">2018</p>
                            <p style="font-size: 12px;">Réalisation de devis classification et archivage des dossiers, création graphique,
                                prospection salons et évènements</p>
                        </div>
                        <div class="col-lg">
                            <i class="fa fa-star fa-2x"></i><h2>Community Manager</h2>
                            <p>- Renew TV - Association de Streamer | En ligne</p>
                            <p style="font-size: 12px;">2017</p>
                            <p style="font-size: 12px;">Rédaction d’article lié au domaine du jeu vidéo et de l’E-Sport, gestion des réseaux sociaux, gestion de l’image internet de l’association</p>
                        </div>
                        <div class="col-lg">
                            <i class="fa fa-star fa-2x"></i><h2>Gestionnaire-Administrative</h2>
                            <p>Stage 1 mois</p>
                            <p>- Mairie Des Viviers-Du-Lac | Les Viviers-Du-Lac</p>
                            <p style="font-size: 12px;">2015</p>
                            <p style="font-size: 12px;">Recrutements, archivage des dossiers municipaux, gestion des finances, rédaction des délibérations des conseils municipaux et permis de construire</p>
                        </div>
                        <div class="col-lg">
                            <i class="fa fa-star fa-2x"></i><h2>Gestionnaire-Administrative</h2>
                            <p>Stage 4 mois</p>
                            <p>- Family Sphere | Chambéry</p>
                            <p style="font-size: 12px;">2012/2015</p>
                            <p style="font-size: 12px;">Gestion des fiches de salaires, recrutement de nouvelles nourrices, gestion des fichiers clients et salariés, planning, classification et archivage</p>
                        </div>









                        </div>
                    </div>
                </div>
                </div>



    </div>
    </div>
</body>
</html>